# Summary #

Simple algorithms and data structures that I've implemented for practice. 
Usually based off Cormen's Introduction to Algorithms.

Under "learning" are practice applications from following these courses on Udemy:

* Rock the JVM! Scala and Functional Programming for Beginners
* Modern React with Redux [2019 Update]
* Learn How To Code: Google's Go (golang) Programming Language

### Practice Projects ###

* **Knuth-Morris-Pratt String Matching** 

> Returns the index of the first concurrence of a pattern in a given text, based on the above algorithm. (_Java_)


* **Priority queue** 

> Implemented using a binary heap. Also includes an implementation of the heapsort algorithm (_Java_).


* **Queue - Array implementation** 

> I was asked during a technical to write a queue using an array. I bungled that question during the interview, so I did it at home. (_Java_).