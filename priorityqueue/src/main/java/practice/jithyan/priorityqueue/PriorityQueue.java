package practice.jithyan.priorityqueue;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;

/**
 * My simple implementation of a PriorityQueue, using a binary heap.
 * 
 * @author Moody
 *
 * @param <T>
 */
public class PriorityQueue<T>{
    private ArrayList<T> array;
    private Comparator<T> comparator;

    public PriorityQueue(Comparator<T> c) {
        array = new ArrayList<>();
        comparator = c;
    }
    
    public PriorityQueue(int initialCapacity, Comparator<T> c) {
        array = new ArrayList<>(initialCapacity);
        comparator = c;
    }
    
    public PriorityQueue(Collection<T> collection, Comparator<T> c) {
    	array = new ArrayList<>(collection.size());
    	comparator = c;
    	array.addAll(collection);
    	buildMaxHeap();
    }
    
    
    /*
     * Returns the head of the priority queue with the largest key, but does not remove.
     */
    public T peek() {
    	if (array.isEmpty()) {
    		return null;
    	} else {    		
    		return this.array.get(0);
    	}
    }
    
    
    /*
     * Returns and removes the head of the queue with the largest key.
     */
    public T remove() {    	
    	if (array.isEmpty()) {
    		return null;
    	} else {	
	    	swapElements(0, array.size() - 1);
	    	T head = this.array.remove(array.size() - 1);
	    	maintainMaxHeapAtSubTree(0, array.size() - 1);
	    	
	    	return head;
    	}
    }
    
    
    /*
     * Adds an element into the appropriate position based on its key.
     */
    public boolean add(T elem) {
    	if (this.array.add(elem)) {
	    	for (int i = this.array.size() - 1; 
	    			(i > 0) && 
	    			(comparator.compare(array.get(parent(i)), array.get(i)) < 0); 
	    			i = parent(i)) {
	    		swapElements(i, parent(i));
	    	}
	    	return true;
    	} else {
    		return false;
    	}
    }
    
    
    /*
     * Returns the number of elements in the priority queue.
     */
    public int size() {
    	return this.array.size();
    }
    
    
    /*
     * My Implementation of the heapsort algorithm.
     */
    private void sort() {
    	buildMaxHeap();
    	int heapsize = array.size();
    	for (int i = array.size() - 1; i > 0; i--) {
    		swapElements(0, i);    		
    		heapsize--;
    		maintainMaxHeapAtSubTree(0, heapsize);
    	}
    }
    
    /*
     * In a bottom-up manner, build a max heap (where every node's parent 
     * is larger than itself).  
     */
    private void buildMaxHeap() {
    	for (int i = parent(array.size()); i >= 0; i--) {
    		maintainMaxHeapAtSubTree(i, array.size());
    	}
    }    
    
    /*
     * At a given "node" i, ensure its subtree maintains
     * the max heap property, such that the values at nodes (left(i) and right(i)) <= i);
     */
    private void maintainMaxHeapAtSubTree(int i, int heapSize) {
    	int largest = i;
    	do {
    		i = largest;
    		int left = left(i);
    		int right = right(i);
    		
    		if (left < heapSize) {
    			if (comparator.compare(array.get(left), array.get(i)) > 0) {
    				largest = left;
    			} else {
    				largest = i;
    			}
    		}
    		
    		if (right < heapSize) {
    			if (comparator.compare(array.get(right), array.get(largest)) > 0) {
    				largest = right;
    			}
    		}
    		
    		if (largest != i) {
    			swapElements(i, largest);
    		}
    	} while (largest != i);    	
    }
    
    /*
     * Helper method to swap two elements in an array.
     */
    private void swapElements(int i, int j) {
    	T temp = array.get(i);
    	array.set(i, array.get(j));
    	array.set(j, temp);
    }


    /*
     * Returns the parent "node" of an array index.
     */
    private static int parent(int i) {
    	return (i - 1) / 2;
    }
    

    /*
     * Returns the left "node" of an array index.
     */
    private static int left(int i) {
        return (i << 1) + 1;
    }
    

    /*
     * Returns the right "node" of an array index.
     */
    private static int right(int i) {
        return (i << 1) + 2;
    }

}
