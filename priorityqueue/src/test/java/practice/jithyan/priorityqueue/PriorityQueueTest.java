package practice.jithyan.priorityqueue;

import java.util.ArrayList;
import java.util.Comparator;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class PriorityQueueTest {
	private PriorityQueue<Integer> pq;
	private Comparator<Integer> comparator;
	private ArrayList<Integer> intArray;
	
	@Before
	public void setup() {
		intArray = new ArrayList<>();
		comparator = new Comparator<Integer>() {
			@Override
			public int compare(Integer x, Integer y) {
				return x - y;
			}			
		};
	}

	
	@Test
	public void dequeueRemovesCorrectNodeFromQueueSize10() {
		prepareIntArray(10);
		this.pq = new PriorityQueue<>(this.intArray, this.comparator);
		int result = pq.remove();
		this.intArray.sort(comparator);
		int expected = intArray.get(9);

		assertTrue("expected: " + expected + ", result: " + result, result == expected);
		assertTrue("expected size: 9, actual size: " + pq.size(), pq.size() == 9);
	}
	
	
	@Test
	public void dequeueRemovesCorrectNodeFromQueueSize1() {
		prepareIntArray(1);
		this.pq = new PriorityQueue<>(this.intArray, this.comparator);
		int result = pq.remove();
		this.intArray.sort(comparator);
		int expected = intArray.get(0);

		assertTrue("expected: " + expected + ", result: " + result, result == expected);
		assertTrue("expected size: 0, actual size: " + pq.size(), pq.size() == 0);
	}
	
	
	@Test
	public void dequeueReturnsNullForEmptyQueue() {
		this.pq = new PriorityQueue<>(this.intArray, this.comparator);
		Integer result = pq.remove();
		Integer expected = null;

		assertTrue("expected: " + expected + ", result: " + result, result == expected);
		assertTrue("expected size: 0, actual size: " + pq.size(), pq.size() == 0);
	}
	
	
	@Test
	public void enqueueCorrectlyAddsToEmptyQueue() {
		this.pq = new PriorityQueue<>(comparator);
		this.pq.add(10);
		assertTrue(pq.size() == 1);
		assertTrue(pq.peek() == 10);
	}
	
	
	@Test
	public void enqueueReturnsCorrectHeadWhenASmallerNumberThanMaxIsAdded() {
		prepareIntArray(25);
		this.pq = new PriorityQueue<>(intArray, comparator);
		this.pq.add(-1);
		intArray.sort(comparator);
		int expected = intArray.get(24);
		assertTrue(pq.size() == 26);
		assertTrue(pq.peek() == expected);
	}
	
	
	@Test
	public void enqueueReturnsCorrectHeadWhenALargerNumberThanCurrentMaxIsAdded() {
		prepareIntArray(100);
		this.pq = new PriorityQueue<>(intArray, comparator);
		this.pq.add(1000);;
		assertTrue(pq.size() == 101);
		assertTrue(pq.peek() == 1000);
	}

	
	private void prepareIntArray(int length) {
		for (int i = 0; i < length; i++) {
			this.intArray.add((int) (Math.random() * 100));
		}
	}
}
