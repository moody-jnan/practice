package practice.jithyan.kmpstringmatching;

/*
 * My simple implementation of the Knuth-Morris-Pratt string
 * matching algorithm.
 */
public class KMPStringMatcher {
	
	private KMPStringMatcher() {}
	
	/*
	 * Finds the first occurrence of a pattern in a given text.
	 * Returns -1 f the pattern does not occur, or if the pattern or text are empty strings. 
	 */
	public static int find(String text, String pattern) {
		if (text == null || pattern == null) {
			throw new IllegalArgumentException("Arguments cannot be null");
		}
		
		if (text.isEmpty() || pattern.isEmpty() || pattern.length() > text.length()) {
			return -1;
		}
		
		int result = -1;
		int[] prefixInfo = computePrefixFunction(pattern);
		int q = -1;
		
		for (int i = 0; i < text.length(); i++) {
			while ((q >= 0) && (pattern.charAt(q + 1) != text.charAt(i))) {
				q = prefixInfo[q];
			}
			
			if (pattern.charAt(q + 1) == text.charAt(i)) {
				q = q + 1;
			}

			if (q == pattern.length() - 1) {
				result =  (i - pattern.length()) + 1;
				return result;
			}
		}
		
		return result;
	}
	
	
	/*
	 * A precomputation function on the pattern P, that records information
	 * on the shifts of a prefix Pk of P that form a proper suffix of Pq.
	 * This information is used to determine shifts when matching the pattern
	 * to some text.
	 */
	private static int[] computePrefixFunction(String pattern) {
		int[] prefixInfo = new int[pattern.length()];
		
		prefixInfo[0] = -1;
		int k = -1;
		
		for (int q = 1; q < pattern.length(); q++) {
			while ((k >= 0) && (pattern.charAt(k + 1) != pattern.charAt(q))) {
				k = prefixInfo[k];
			}
			
			if (pattern.charAt(k + 1) == pattern.charAt(q)) {
				k = k + 1;
			}
			
			prefixInfo[q] = k;
		}
		
		return prefixInfo;
	}
}
