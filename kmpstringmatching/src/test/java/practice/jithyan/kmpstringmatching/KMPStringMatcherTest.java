package practice.jithyan.kmpstringmatching;

import static org.junit.Assert.*;

import org.junit.Test;

public class KMPStringMatcherTest {
	
	@Test
	public void findReturnsMinusOneGivenTwoEmptyStrings() {
		int expected = -1;
		int result = KMPStringMatcher.find("", "");
		assertTrue(result == expected);
	}
	
	
	@Test
	public void findReturnsMinusOneGivenEmptyTextAndANonEmptyPattern() {
		int expected = -1;
		int result = KMPStringMatcher.find("", "hello") ;
		assertTrue(result == expected);
	}
	
	
	@Test
	public void findReturnsMinusOneGivenNonEmptyTextAndAnEmptyPattern() {
		int expected = -1;
		int result = KMPStringMatcher.find("how now brown cow\n", "");
		assertTrue(result == expected);
	}	
	
	
	@Test
	public void findReturnsZeroGivenEqualLengthNonZeroStringsAsInput() {
		String test = "ababaca";
		int expected = 0;
		int result = KMPStringMatcher.find(test, test);;
		assertTrue(result == expected);
	}
	
	
	@Test
	public void findReturnsCorrectIndexGivenPatternLessThanTextLengthAndIsPresent() {
		String text = "\nHow I Met Your Mum";
		String pattern = "Your";
		int result = KMPStringMatcher.find(text, pattern);
		int expected = 11;
		assertTrue(result == expected);
	}

	
	@Test
	public void findReturnsMinusOneGivenPatternLessThanTextLengthAndIsNotPresent() {
		String text = "\nHow I Met Your Mum";
		String pattern = "your";
		int result = KMPStringMatcher.find(text, pattern);
		int expected = -1;
		assertTrue(result == expected);
	}
	
	
	@Test
	public void findReturnsMinusOneGivenPatternGreaterThanTextLength() {
		String text = "\nHow I Met Your Mum";
		String pattern = "\nHow I Met Your Mum!";
		int expected = -1;
		int result = KMPStringMatcher.find(text, pattern);
		assertTrue(result == expected);
	}
}
