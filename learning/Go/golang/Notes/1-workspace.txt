Standard go workspace organisation:

/bin --> compiled binaries
/pkg --> packages used by program, archived, compiled (provided if no change has occurred)
/src --> source code
/src/github.com --> domain
/src/github.com/username/
/src/github.com/username/folderwithcode for project/repo


Use "go get" to fetch packages.

Environment vairables:
GOPATH --> points to your workspace
GOROOT --> points to your binary installation of golang


In windows go to the usual place to add environment variables.
In linux:
export GOPATH=`pwd`
export PATH="$PATH:$GOPATH" //to find your binary

(you can verify using echo $PATH)

Or you can configure .bash_profile (or create) & .bashrc
Use above code