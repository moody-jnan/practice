# Spring Boot

* spring.io
* Add a parent tag to your POM with a reference to Spring Boot Starter
* Add dependencies: the default web servlet is the Spring Boot Web Starter. Otherwise use Jetty or Undertow.


* The Spring Boot Starter Parent is a Bill of Materials (BOM) that automatically manages the versions of dependencies for you.
* Therefore when adding dependencies already mentioned in the BOM, you omit the **version** as it already includes the *most suitable* version that's compatible with your version of Spring.


![How Spring Boot Works](./how-sb-works.PNG)

* web.xml is not required as Spring configures it for you.

![How Spring Boot Works 2](./how-sb-works2.PNG)

* Instead of using XML to configure Spring, use Java.
* `@EnableAutoConfiguration` tells Spring Boot to scan for any projects that are sub-projects of Spring or compatible with Spring and wire them up and configure them automatically.


## Why Containerless Deployments (as preferred by Spring Boot)

![Why Containerless](./why-containerless-deployments.PNG)


## Spring Boot Default Static Content Locations

![Locations](./sb-default-content-loc.PNG)


## What takes place when you startup Spring Boot

![Startup](./startup-overview.PNG)

![Config](./app-config.PNG)

* Properties can be configured externally. If you want to have it in YAML, you need a dependency on classpath.
* In a Maven project, the root classpath is kept in `src/main/resources`.
* You can have different properties for each environment in the format: `application-{env-name}.properties`
* The application properties and environment properties work hand in hand. You override your application properties (or add new ones) in the environment properties file.
* To run a specific application environment, add the following to your run configuration or command line:

![Run Config](./run-config.PNG)

`-Dspring.profiles.active-test`

To see common application properties, go [here](https://docs.spring.io/spring-boot/docs/current/reference/html/common-application-properties.html)
