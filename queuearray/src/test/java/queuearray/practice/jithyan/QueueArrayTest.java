package queuearray.practice.jithyan;

import org.junit.Test;
import org.hamcrest.core.StringContains;

import static org.junit.Assert.*;

public class QueueArrayTest {
	
	@Test
	public void youCantCreateAQueueWithSizeLessThan1() {
		try {
			new Queue<Integer>(0);
			fail("No IllegalArgumentException was thrown.");
		} catch(IllegalArgumentException e) {
			assertThat(e.getMessage(), StringContains.containsString("greater than zero"));
		} 
	}
	
	
	@Test
	public void addNodeToEmptyQueue() {
		Queue<Integer> q = new Queue<>();
		q.enqueue(1);
		assertEquals(1, q.numElements());
		assertEquals(new Integer(1), q.dequeue());		
	}
	
	
	@Test
	public void addNodeToNonEmptyQueue() {
		Queue<Integer> q = new Queue<>();
		q.enqueue(1);
		assertEquals(1, q.numElements());
		assertEquals(new Integer(1), q.dequeue());		
	}
	
	
	@Test
	public void removingNodeFromEmptyNodeReturnsNull() {
		Queue<Integer> q = new Queue<>();
		assertEquals(null, q.dequeue());	
	}
	
	
	@Test
	public void nodesRemovedFromQueueAreInTheCorrectFIFOOrder() {
		Queue<Integer> q = new Queue<>();
		q.enqueue(1);
		q.enqueue(10);
		q.enqueue(20);
		q.enqueue(50);
		q.enqueue(0);
		assertEquals(5, q.numElements());
		assertEquals(new Integer(1), q.dequeue());
		assertEquals(new Integer(10), q.dequeue());	
		assertEquals(new Integer(20), q.dequeue());	
		assertEquals(new Integer(50), q.dequeue());	
		assertEquals(new Integer(0), q.dequeue());	
		assertEquals(null, q.dequeue());	
	}

	
	@Test
	public void nodesRemovedFromQueueAreInTheCorrectFIFOOrderAfterItsCapacityIsExpanded() {
		Queue<Integer> q = new Queue<>(4);
		q.enqueue(1);
		q.enqueue(10);
		q.enqueue(20);
		q.enqueue(50);
		
		q.dequeue();
		q.dequeue();
		q.dequeue();
		
		q.enqueue(1);
		q.enqueue(10);
		q.enqueue(20);
		q.enqueue(0);
		q.enqueue(-99);
		q.enqueue(2);
		
		assertEquals(7, q.numElements());
		
		assertEquals(new Integer(50), q.dequeue());	
		assertEquals(new Integer(1), q.dequeue());
		assertEquals(new Integer(10), q.dequeue());	
		assertEquals(new Integer(20), q.dequeue());	
		assertEquals(new Integer(0), q.dequeue());	
		assertEquals(new Integer(-99), q.dequeue());	
		assertEquals(new Integer(2), q.dequeue());	
		
		assertEquals(null, q.dequeue());	
	}
	
	
	@Test
	public void nodesRemovedFromQueueAreInTheCorrectFIFOOrderAfterInterleavedEnqueuesAndDequeues() {
		Queue<Integer> q = new Queue<>(3);
		q.enqueue(1);
		q.enqueue(2);
		q.enqueue(3);
		q.dequeue();
		q.dequeue();
		q.enqueue(-1);
		q.enqueue(-2);
		assertEquals(3, q.numElements());
		assertEquals(new Integer(3), q.dequeue());
		assertEquals(new Integer(-1), q.dequeue());	
		assertEquals(new Integer(-2), q.dequeue());		
		assertEquals(null, q.dequeue());	
	}
}
