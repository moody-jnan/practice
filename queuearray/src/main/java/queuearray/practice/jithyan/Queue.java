package queuearray.practice.jithyan;

/*
 * My implementation of a queue using an array.
 * I was asked to implement this in a technical interview, which I bungled on the interview day.
 */
public class Queue<E>
{
	private static final int DEFAULT_SIZE = 15;
	private E[] array;
	private int count;
	private int first;
	
	@SuppressWarnings("unchecked")
	public Queue() {
		this.array = (E[]) new Object[DEFAULT_SIZE];
		this.count = 0;
	}
	
	
	/*
	 * Create a new queue, with a given initial size of its array.
	 */
	@SuppressWarnings("unchecked")
	public Queue(int size) {
		if (size > 0) {
			this.array = (E[]) new Object[size];
			this.count = 0;
		} else {
			throw new IllegalArgumentException("Size must be greater than zero.");
		}
	}
	
	
	/*
	 * Add a new element to the queue.
	 */
	public void enqueue(E item) {
		if (item == null) {
			throw new IllegalArgumentException("Cannot insert null element");
		}
		
		if (count >= array.length) {
			resizeArray();
		}
		
		array[(count + first) % array.length] = item;
		count++;
	}
	
	
	/*
	 * Returns the first element of the queue.
	 * Returns null if the queue is empty.
	 */
	public E dequeue() {
		if (count == 0) {
			return null;
		} else {
			E elem = array[first];
			array[first] = null;
			first = (first + 1) % array.length;
			count--;
			
			return elem;
		}
	}
	
	
	/*
	 * Returns the number of elements in the queue.
	 */
	public int numElements() {
		return this.count;
	}
	
	
	@SuppressWarnings("unchecked")
	/*
	 * When the queue's array needs to be expanded, this method is to be called.
	 * It returns a new array containing the old array's elements, while maintaining its order.
	 * The first element of the queue is at position 0 of the array.
	 */
	private void resizeArray() {
		E[] newQueue = (E[]) new Object[this.array.length + DEFAULT_SIZE];
		
		int oldQueueCurrentIndex = first;
		int newQueueCurrentIndex = 0;
		
		while (oldQueueCurrentIndex < this.array.length) {
			newQueue[newQueueCurrentIndex++] = this.array[oldQueueCurrentIndex++];
		}
		
		oldQueueCurrentIndex = 0;
		while (oldQueueCurrentIndex < this.first) {
			newQueue[newQueueCurrentIndex++] = this.array[oldQueueCurrentIndex++];
		}
		
		this.array = newQueue;
		this.first = 0;
	}
}
